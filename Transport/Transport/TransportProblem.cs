﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Transport
{
    [Serializable]
    public class TransportProblem
    {
        List<float> providers = new List<float>();//постачальники
        List<float> customers = new List<float>();//споживачі
        float[,] costs;//матриця цін
        float[,] supportPlan;//матриця перевезень, опорний план
        //потенціали
        float[] U;
        float[] V;
        public float[,] Dij;
        private enum SolvingCondition { None = 0, Ok = 1, AddedRow = 2, AddedColumn = 3 };
        SolvingCondition solvC = SolvingCondition.None;
        public TransportProblem() { }
        //Властивості доступу
        public List<float> Providers
        {
            get { return providers; }
            set { providers = value; }
        }
        public List<float> Customers
        {
            get { return customers; }
            set { customers = value; }
        }
        public float[,] Costs
        {
            get { return costs; }
            set { costs = value; }
        }
        public float[,] SupportPlan
        {
            get { return supportPlan; }
            set { supportPlan = value; }
        }
        public float[] UPotential { get { return U; } }
        public float[] VPotential { get { return V; } }
        public float[,] Potencials { get { return Dij; } }
        //--------------------------------------------
        public void AddProviders(params float[] values)
        {
            foreach (float d in values)
                providers.Add(d);
        }
        public void AddCustomers(params float[] values)
        {
            foreach (float d in values)
                customers.Add(d);
        }
        //перевірка на критерій розв'язку(закрита чи відкрита задача)
        public bool CheckSolvingCondition()
        {
            if (providers.Sum() == customers.Sum())
            {
                solvC = SolvingCondition.Ok;
                return true;
            }
            else
                return false;
        }

        //якщо задача відкрита(сума споживачів не дорівнює сумі постачальників), то додається стовпець чи рядок
        public void AddToSolvingCondition()
        {
            if (providers.Sum() > customers.Sum())
            {
                customers.Add(providers.Sum() - customers.Sum());
                if (costs != null)
                {
                    float[,] newCost = new float[providers.Count, customers.Count];
                    for (int i = 0; i < providers.Count; i++)
                    {
                        for (int j = 0; j < customers.Count - 1; j++)
                            newCost[i, j] = costs[i, j];
                        newCost[i, customers.Count - 1] = 0;
                    }
                    costs = newCost;
                }
                solvC = SolvingCondition.AddedColumn;
            }
            if (providers.Sum() < customers.Sum())
            {
                providers.Add(customers.Sum() - providers.Sum());
                if (costs != null)
                {
                    float[,] newCost = new float[providers.Count, customers.Count];
                    for (int i = 0; i < customers.Count; i++)
                    {
                        for (int j = 0; j < providers.Count - 1; j++)
                            newCost[j, i] = costs[j, i];
                        newCost[providers.Count - 1, i] = 0;
                    }
                    costs = newCost;
                }
                solvC = SolvingCondition.AddedRow;
            }
        }

        //північно-західний план
        public void NW_SupportPlan()
        {
            float[] providersCopy = providers.ToArray();
            float[] customersCopy = customers.ToArray();
            supportPlan = new float[providers.Count, customers.Count];
            int i = 0, j = 0;
            //поки всі елементи в масивах не будуть дорівнювати 0
            while (!(Array.TrueForAll(providersCopy, delegate (float x) { return x == 0; })
                && Array.TrueForAll(customersCopy, delegate (float x) { return x == 0; })))
            {
                float difference = (float)Math.Min(providersCopy[i], customersCopy[j]);
                //if (difference < 0.00001f)
                //    difference = 0;
                supportPlan[i, j] = difference;
                providersCopy[i] -= difference;
                customersCopy[j] -= difference;

                if (providersCopy[i] < 0.00001f)
                    providersCopy[i] = 0;

                if (customersCopy[j] < 0.00001f)
                    customersCopy[j] = 0;

                //if ((providersCopy[i] == 0) && (customersCopy[j] == 0) && (j + 1 < customersCopy.Length))
                //    supportPlan[i, j + 1] = 0;
                if (providersCopy[i] == 0)
                    i++;
                if (customersCopy[j] == 0)
                    j++;
            }
        }

        //розрахунок загальної вартості перевезень
        public float CostsSumByPlan()
        {
            if (supportPlan == null || costs == null)
                return -1;
            float sum = 0;
            for (int i = 0; i < providers.Count; i++)
                for (int j = 0; j < customers.Count; j++)
                    sum += costs[i, j] * supportPlan[i, j];
            return sum;
        }

        //Опорний план за мінімальною вартістю
        public void MinCost_SupportPlan()
        {
            float[,] costsCopy = new float[providers.Count, customers.Count];
            for (int i = 0; i < providers.Count; i++)
                for (int j = 0; j < customers.Count; j++)
                    costsCopy[i, j] = costs[i, j];
            float[] providersCopy = providers.ToArray();
            float[] customersCopy = customers.ToArray();
            supportPlan = new float[providers.Count, customers.Count];
            int imin = 0, jmin = 0;

            while (!(Array.TrueForAll(providersCopy, delegate (float x) { return x == 0; })
    && Array.TrueForAll(customersCopy, delegate (float x) { return x == 0; })))
            {
                float min = float.MaxValue;
                for (int i = 0; i < providers.Count; i++)
                    for (int j = 0; j < customers.Count; j++)
                        if (min > costsCopy[i, j])
                        {
                            min = costsCopy[i, j];
                            imin = i;
                            jmin = j;
                        }
                float difference = Math.Min(providersCopy[imin], customersCopy[jmin]);
                //if (difference < 0.00001f)
                //    difference = 0;
                costsCopy[imin, jmin] = float.MaxValue;
                #region
                //if (providersCopy[imin] == 0)
                //{
                //    for (int j= 0; j < customers.Count; j++)
                //    {
                //        costsCopy[imin, j] = float.MaxValue;
                //        supportPlan[imin, j] = 0;
                //    }
                //}
                //if (customersCopy[jmin] == 0)
                //{
                //    for (int i = 0; i < providers.Count; i++)
                //    {
                //        costsCopy[i, jmin] = float.MaxValue;
                //        supportPlan[i, jmin] = 0;
                //    }
                //}
                #endregion
                supportPlan[imin, jmin] = difference;
                providersCopy[imin] -= difference;
                customersCopy[jmin] -= difference;

                if (providersCopy[imin] < 0.00001f)
                    providersCopy[imin] = 0;

                if (customersCopy[jmin] < 0.00001f)
                    customersCopy[jmin] = 0;

            }
        }

        //перевірка на вирожденісь рішення задачі
        public void CheckDegenerate()
        {
            int amountOfNonZero = 0;
            for (int i = 0; i < providers.Count; i++)
                for (int j = 0; j < customers.Count; j++)
                    if (supportPlan[i, j] != 0)
                        amountOfNonZero++;
            if (amountOfNonZero >= (customers.Count + providers.Count - 1))
                return;
            else
            {
                const float needToAdd = 0.00001f;
                for (int i = 0; i < providers.Count; i++)
                    for (int j = 0; j < customers.Count; j++)
                        if (supportPlan[i, j] == 0)
                        {
                            supportPlan[i, j] = needToAdd;
                            providers[i] += needToAdd;
                            customers[j] += needToAdd;
                            amountOfNonZero++;
                            if (amountOfNonZero > (customers.Count + providers.Count - 1))
                                return;
                            break;
                        }
            }
        }

        //метод розрахунків потенціалів
        public void PotentailsCalculate()
        {
            U = new float[providers.Count];
            V = new float[customers.Count];
            bool[] Ubool = new bool[providers.Count];
            bool[] Vbool = new bool[customers.Count];

            List<Point> NonZeroSupPlanValues = AddNonZeroValues();
            NonZeroSupPlanValues.Sort(new PointComparer().Compare);

            Point p = NonZeroSupPlanValues.First();
            U[(int)p.X] = 0;
            V[(int)p.Y] = costs[(int)p.X, (int)p.Y];

            PotentialRecursion(ref U, ref V, ref Ubool, ref Vbool, (int)p.Y, (int)p.X, true);

            if (!(Array.TrueForAll(Ubool, delegate (bool x) { return x == true; })
                && Array.TrueForAll(Vbool, delegate (bool x) { return x == true; })))

                PotentialRecursion(ref U, ref V, ref Ubool, ref Vbool, (int)p.Y, (int)p.X, false);

        }

        //цей метод запускається для рекурсивного пошуку потенціалів
        private void PotentialRecursion(ref float[] U, ref float[] V, ref bool[] Ubool, ref bool[] Vbool, int i, int j, bool line)
        {
            if (line)
            {
                for (int k = 0; k < customers.Count; k++)
                    if (supportPlan[i, k] != 0 && Vbool[k] != true)
                    {
                        V[k] = costs[i, k] - U[i];
                        Vbool[k] = true;

                        if (!(Array.TrueForAll(Ubool, delegate (bool x) { return x == true; })
                && Array.TrueForAll(Vbool, delegate (bool x) { return x == true; })))

                            PotentialRecursion(ref U, ref V, ref Ubool, ref Vbool, i, k, false);
                    }
            }
            else
            {
                for (int k = 0; k < providers.Count; k++)
                    if (supportPlan[k, j] != 0 && Ubool[k] != true)
                    {
                        U[k] = costs[k, j] - V[j];
                        Ubool[k] = true;

                        if (!(Array.TrueForAll(Ubool, delegate (bool x) { return x == true; })
    && Array.TrueForAll(Vbool, delegate (bool x) { return x == true; })))

                            PotentialRecursion(ref U, ref V, ref Ubool, ref Vbool, k, j, true);
                    }
            }
        }

        //перевіряється рішення на оптимальність,
        //по посиланню повертає масив дельта,
        //номер рядка і стовбця - де знаходиться найменьший елемент( менший за нуль) і сам менший елемент
        public bool CheckOptimalSolution(out float[,] Dij, out List<Point> IJmin, int chooseMin = 0)
        {
            Dij = new float[providers.Count, customers.Count];
            bool optimalSolution = true;
            float min = float.MaxValue;
            IJmin = new List<Point>();
            for (int i = 0; i < providers.Count; i++)
                for (int j = 0; j < customers.Count; j++)
                    if (supportPlan[i, j] != 0)
                        Dij[i, j] = 0;
                    else
                    {
                        Dij[i, j] = costs[i, j] - (U[i] + V[j]);
                        if (Dij[i, j] < 0)
                        {
                            optimalSolution = false;
                            if (Dij[i, j] == min)
                                IJmin.Add(new Point(j, i));
                            if (min > Dij[i, j])
                            {
                                min = Dij[i, j];
                                IJmin.Clear();
                                IJmin.Add(new Point(j, i));
                            }
                        }
                    }
            return optimalSolution;
        }
        public bool CheckOptimalSolution()
        {
            Dij = new float[providers.Count, customers.Count];
            for (int i = 0; i < providers.Count; i++)
                for (int j = 0; j < customers.Count; j++)
                    if (supportPlan[i, j] != 0)
                        Dij[i, j] = 0;
                    else
                    {
                        Dij[i, j] = costs[i, j] - (U[i] + V[j]);
                        if (Dij[i, j] < 0)
                            return false;
                    }
            return true;

        }
        //додає не нульові значення опорного плану до списку і повертає його
        private List<Point> AddNonZeroValues()
        {
            List<Point> NonZeroSupPlanValues = new List<Point>();
            if (providers.Count == 1)
                for (int i = 0; i < customers.Count; i++)
                    NonZeroSupPlanValues.Add(new Point(i, 0));
            else if (customers.Count == 1)
                for (int i = 0; i < providers.Count; i++)
                {
                    if (!NonZeroSupPlanValues.Contains(new Point(0, i)))
                        NonZeroSupPlanValues.Add(new Point(0, i));
                }
            for (int j = 0; j < providers.Count - 1; j++)
                for (int i = 0; i < customers.Count - 1; i++)
                    if (supportPlan[j, i] != 0)
                        NonZeroSupPlanValues.Add(new Point(j, i));
            return NonZeroSupPlanValues;
        }

        //серіалізує об'єкт в оперативну пам'ять, повертає десеріалізовану повну(глибоку) копію об'єкта
        public TransportProblem DeepCopy()
        {
            using (System.IO.MemoryStream mem = new System.IO.MemoryStream())
            {
                BinaryFormatter format = new BinaryFormatter();
                format.Serialize(mem, this);
                mem.Seek(0, System.IO.SeekOrigin.Begin);
                return format.Deserialize(mem) as TransportProblem;
            }
        }

        //серіалізація в файл
        public bool Save(string filename)
        {
            try
            {
                using (FileStream file = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    BinaryFormatter ser = new BinaryFormatter();
                    ser.Serialize(file, this);
                }
            }
            catch { return false; }
            return true;
        }

        //серіалізація з файла
        public bool Load(string filename)
        {
            try
            {
                using (FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter ser = new BinaryFormatter();
                    TransportProblem temp = ser.Deserialize(file) as TransportProblem;
                    this.providers = temp.providers;
                    this.customers = temp.customers;
                    this.costs = temp.costs;
                    this.supportPlan = temp.supportPlan;
                    this.U = temp.U;
                    this.V = temp.V;
                    this.solvC = temp.solvC;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        //клас, для пошуку циклу. Трохи говнокод, можна спростити і скоротити
        [Serializable]
        public class TranportPoint
        {
            //напрями руху з точки циклу
            private bool LeftWay { get; set; }
            private bool UpWay { get; set; }
            private bool RightWay { get; set; }
            private bool DownWay { get; set; }
            bool isroot = false;
            //стани поточного руху з точки циклу
            private enum WayState { None, Left, Up, Right, Down }
            private WayState current;
            //координати корня(початку) циклу
            private Point Root = new Point();
            //наступний елемент в циклі
            private TranportPoint Children;
            //попередній елемент в циклі
            private TranportPoint Father;
            //поточні координати точки
            private Point curPoint = new Point();
            TransportProblem tp;
            public TranportPoint(Point root, TransportProblem t)
            {
                Father = null;
                curPoint = Root = root;
                LeftWay = UpWay = RightWay = DownWay = true;
                current = WayState.None;
                tp = t;
                isroot = true;
                InitWays();
            }
            private TranportPoint(Point root, TranportPoint father, TransportProblem t, Point curP)
            {
                Father = father;
                Root = root;
                curPoint = curP;
                tp = t;
                current = WayState.None;
                LeftWay = UpWay = RightWay = DownWay = true;
                InitWays();
            }

            //обмежує напрямки, по яким рухатись не можна
            private void InitWays()
            {
                if (curPoint.X == 0)
                    LeftWay = false;
                if (curPoint.X == tp.customers.Count - 1)
                    RightWay = false;
                if (curPoint.Y == 0)
                    UpWay = false;
                if (curPoint.Y == tp.providers.Count - 1)
                    DownWay = false;
                if (Father != null && (Father.current == WayState.Left || Father.current == WayState.Right))
                    LeftWay = RightWay = false;
                if (Father != null && (Father.current == WayState.Up || Father.current == WayState.Down))
                    UpWay = DownWay = false;
            }

            //перевірка на проходження по вже існуючій точці циклу
            private bool checkCyclePoints(Point p)
            {
                TranportPoint iterator = this;
                while (iterator.Father != null)
                {
                    if (p == iterator.curPoint)
                        return false;
                    iterator = iterator.Father;
                }
                return true;
            }

            //перевіряє на те що початок шляху заходиться під 90 градусним кутом до кінця шляху
            private bool checkRootWayError()
            {
                TranportPoint RootIter = this; ;
                while (RootIter.Father != null)
                    RootIter = RootIter.Father;
                if ((RootIter.current == WayState.Left || RootIter.current == WayState.Right) && (Father.current == WayState.Up || Father.current == WayState.Down))
                    return true;
                if ((RootIter.current == WayState.Up || RootIter.current == WayState.Down) && (Father.current == WayState.Left || Father.current == WayState.Right))
                    return true;
                return false;
            }

            //перевірка, чи всі можливі напрямки руху перевірені
            public bool HasWays()
            {
                return LeftWay || UpWay || RightWay || DownWay;
            }

            //пошук циклу, можна розбити на 5 функцій
            public bool Findway()
            {
                if (Root == curPoint && !isroot)
                    if (!checkRootWayError())
                        return false;
                    else
                        return true;
                if (LeftWay)
                {
                    for (int i = (int)curPoint.X - 1; i >= 0; i--)
                        if ((tp.supportPlan[(int)curPoint.Y, i] != 0 || (Root.X == i && Root.Y == curPoint.Y)))
                        {
                            LeftWay = false;
                            if (checkCyclePoints(new Point(i, curPoint.Y)))
                            {
                                current = WayState.Left;
                                Children = new TranportPoint(Root, this, tp, new Point(i, curPoint.Y));
                                if (Children.Findway())
                                    return true;
                            }
                        }
                        else LeftWay = false;
                }
                if (UpWay)
                {
                    for (int i = (int)curPoint.Y - 1; i >= 0; i--)
                        if ((tp.supportPlan[i, (int)curPoint.X] != 0 || (Root.X == curPoint.X && Root.Y == i)))
                        {
                            UpWay = false;
                            if (checkCyclePoints(new Point(curPoint.X, i)))
                            {
                                current = WayState.Up;
                                Children = new TranportPoint(Root, this, tp, new Point((int)curPoint.X, i));
                                if (Children.Findway())
                                    return true;
                            }
                        }
                        else UpWay = false;
                }
                if (RightWay)
                {
                    for (int i = (int)curPoint.X + 1; i < tp.customers.Count; i++)
                        if ((tp.supportPlan[(int)curPoint.Y, i] != 0 || (Root.X == i && Root.Y == curPoint.Y)))
                        {
                            RightWay = false;
                            if (checkCyclePoints(new Point(i, curPoint.Y)))
                            {
                                current = WayState.Right;
                                Children = new TranportPoint(Root, this, tp, new Point(i, curPoint.Y));
                                if (Children.Findway())
                                    return true;
                            }
                        }
                        else RightWay = false;
                }
                if (DownWay)
                {
                    for (int i = (int)curPoint.Y + 1; i < tp.providers.Count; i++)
                        if ((tp.supportPlan[i, (int)curPoint.X] != 0 || (Root.X == curPoint.X && Root.Y == i)))
                        {
                            DownWay = false;
                            if (checkCyclePoints(new Point(curPoint.X, i)))
                            {
                                current = WayState.Down;
                                Children = new TranportPoint(Root, this, tp, new Point(curPoint.X, i));
                                if (Children.Findway())
                                    return true;
                            }
                        }
                        else DownWay = false;
                }
                if (Father != null)
                    Father.Children = null;
                return false;
            }

            //повертає список, що містить всі точки циклу
            public List<Point> ReturnWay()
            {
                TranportPoint iterator = this;
                List<Point> result = new List<Point>();
                do
                {
                    result.Add(iterator.curPoint);
                    iterator = iterator.Children;
                }
                while (iterator.curPoint != Root);
                return result;
            }
        }

        //перерозподіляє вантаж по циклу
        public void RollCycle(List<Point> way)
        {
            float min = float.MaxValue;
            //true = positive
            //false = negative
            for (int i = 0; i < way.Count; i++)
            {
                if ((i % 2 != 0) && supportPlan[(int)way[i].Y, (int)way[i].X] < min && supportPlan[(int)way[i].Y, (int)way[i].X] != 0)
                    min = supportPlan[(int)way[i].Y, (int)way[i].X];
            }
            for (int i = 0; i < way.Count; i++)
                if (i % 2 == 0)
                    supportPlan[(int)way[i].Y, (int)way[i].X] += min;
                else
                    supportPlan[(int)way[i].Y, (int)way[i].X] -= min;

        }

        private float test(TransportProblem tp, List<Point> cycle)
        {
            tp.CheckDegenerate();
            tp.PotentailsCalculate();
            tp.RollCycle(cycle);
            return tp.CostsSumByPlan();
        }

        //знаходження оптимального рішення по методу потенціалів
        public void OptimalSolution()
        {
            TranportPoint Way;
            List<Point> min;

            CheckDegenerate();
            PotentailsCalculate();

            while (!CheckOptimalSolution(out Dij, out min))
            {
                int i = 0;
                do
                {
                    Way = new TranportPoint(min[i], this);
                    i++;
                } while (!Way.Findway());
                List<Point> cycle = Way.ReturnWay();
                RollCycle(cycle);
                #region
                //test--------------------------
                //List<List<Point>> amountOfWays = new List<List<Point>>();
                //foreach (Point p in min)
                //{
                //    Way = new TranportPoint(p, this);
                //    do
                //    {
                //        if (Way.Findway())
                //            amountOfWays.Add(Way.ReturnWay());
                //    } while (Way.HasWays());
                //}
                //float minimum = CostsSumByPlan();
                //int minEl = 0;
                //for (int r = 0; r < amountOfWays.Count; r++)
                //{
                //    float compare = test(DeepCopy(), amountOfWays[r]);
                //    if (minimum > compare)
                //    {
                //        minimum = compare;
                //        minEl = r;
                //    }
                //}
                //test(this, amountOfWays[minEl]);
                //test----------------------------
                #endregion
                CheckDegenerate();
                PotentailsCalculate();
            }
        }
    }
    public class PointComparer : IComparer<Point>
    {
        public int Compare(Point one, Point two)
        {
            if (one.X > two.X)
                return 1;
            //if (one.Y > two.Y)
            //    return 3;
            //else
            //    return 4;
            if (one.X < two.X)
                return -1;
            //if (one.Y < two.Y)
            //    return -2;
            //else
            //    return -3;
            else
            {
                if (one.X == two.X && one.Y == two.Y)
                    return 0;
                else
                {
                    if (one.X == two.X && one.Y > two.Y)
                        return 1;
                    if (one.X == two.X && one.Y < two.Y)
                        return -1;
                }
                return 0;
            }
        }
    }
}
