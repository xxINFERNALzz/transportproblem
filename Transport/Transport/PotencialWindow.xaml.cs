﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Transport
{
    /// <summary>
    /// Interaction logic for PotencialWindow.xaml
    /// </summary>
    public partial class PotencialWindow : Window
    {
        public PotencialWindow(TransportProblem tr)
        {
            InitializeComponent();

            TransportProblem.TranportPoint Way;
            List<Point> min;
            int iteration = 0;
            tr.CheckDegenerate();
            tr.PotentailsCalculate();

            //addTab(tr, "start");

            while(!tr.CheckOptimalSolution(out tr.Dij,out min))
            {
                iteration++;

                int i = 0;

                do
                {
                    Way = new TransportProblem.TranportPoint(min[i], tr);
                    i++;
                } while (!Way.Findway());
                List<Point> cycle = Way.ReturnWay();
                //addTab(tr, iteration.ToString(), cycle);
                //------------------------------------------------------
                TabItem tabItem = new TabItem();

                Grid grid = new Grid();
                RowDefinition gridRowDataGrid = new RowDefinition();
                RowDefinition gridRowLable = new RowDefinition();
                gridRowLable.MaxHeight = 25;
                grid.RowDefinitions.Add(gridRowDataGrid);
                grid.RowDefinitions.Add(gridRowLable);

                DataGrid dataGrid = new DataGrid();
                dataGrid.HeadersVisibility = DataGridHeadersVisibility.None;
                DataGridAdapter adapter = new DataGridAdapter(dataGrid, tr);
                adapter.FillPotencialDataGrid(cycle);

                //TODO: fill dataGrid
                grid.Children.Add(dataGrid);
                Grid.SetRow(dataGrid, 0);

                Label LB_optimalCost = new Label();
                LB_optimalCost.Content = "Оптимальна ціна перевезень:" + tr.CostsSumByPlan();
                grid.Children.Add(LB_optimalCost);
                Grid.SetRow(LB_optimalCost, 1);

                tabItem.Content = grid;
                tabItem.Header = "Itaration " + iteration;

                Tab_Control.Items.Add(tabItem);
                //------------------------------------------------------
                tr.RollCycle(cycle);
                tr.CheckDegenerate();
                tr.PotentailsCalculate();

            }
        }

        private void addTab(TransportProblem tr, string iteration, List<Point> cycle = null)
        {
            TabItem tabItem = new TabItem();

            Grid grid = new Grid();
            RowDefinition gridRowDataGrid = new RowDefinition();
            RowDefinition gridRowLable = new RowDefinition();
            gridRowLable.MaxHeight = 25;
            grid.RowDefinitions.Add(gridRowDataGrid);
            grid.RowDefinitions.Add(gridRowLable);

            DataGrid dataGrid = new DataGrid();
            dataGrid.HeadersVisibility = DataGridHeadersVisibility.None;
            DataGridAdapter adapter = new DataGridAdapter(dataGrid, tr);
            adapter.FillPotencialDataGrid(cycle);

            //TODO: fill dataGrid
            grid.Children.Add(dataGrid);
            Grid.SetRow(dataGrid, 0);

            Label LB_optimalCost = new Label();
            LB_optimalCost.Content = "Оптимальна ціна перевезень:" + tr.CostsSumByPlan();
            grid.Children.Add(LB_optimalCost);
            Grid.SetRow(LB_optimalCost, 1);

            tabItem.Content = grid;
            tabItem.Header = "Itaration " + iteration;

            Tab_Control.Items.Add(tabItem);

        }
    }
}
