﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Transport
{
    /// <summary>
    /// Interaction logic for Input.xaml
    /// </summary>
    public partial class Input : Window
    {
        public Input(string l)
        {
            InitializeComponent();
            Label1.Content = l;
        }
        public double[] ResultProvider { get; set; }
        public double[] ResultCustomer { get; set; }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (TB_input.Text == "")
                MessageBox.Show("ВВедіть споживачів!");
            if(TB_inputProviders.Text=="")
                MessageBox.Show("ВВедіть Постачальників!");
            try
            {
                string[] str = TB_input.Text.Split(';');
                ResultProvider = new double[str.Length];
                for (int item = 0; item < str.Length; item++)
                {
                    str[item] = str[item].Trim();
                    ResultProvider[item] = double.Parse(str[item]);
                }
            }
            catch(Exception exp) { MessageBox.Show("Неправильний ввід споживачів!\n"+exp.Message); return; }
            try
            {
                string[] str = TB_input.Text.Split(';');
                ResultCustomer = new double[str.Length];
                for (int item = 0; item < str.Length; item++)
                {
                    str[item] = str[item].Trim();
                    ResultCustomer[item] = double.Parse(str[item]);
                }

            }
            catch(Exception exp) { MessageBox.Show("Неправильний ввід постачальників!\n" + exp.Message); return; }
            DialogResult = true;
        }
    }
}
