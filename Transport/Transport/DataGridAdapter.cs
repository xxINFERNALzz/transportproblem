﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Transport
{
    class DataGridAdapter
    {
        DataGrid dGrid;
        TransportProblem tr;
        public DataGridAdapter(DataGrid d, TransportProblem t)
        {
            dGrid = d;
            tr = t;
        }
        public void FillDataGrid(bool IsEmpty)
        {
            // dGrid.Columns.Clear();
            dGrid.Dispatcher.Invoke(new Action(dGrid.Columns.Clear));
            System.Data.DataTable dtable = new System.Data.DataTable();
            if (!tr.CheckSolvingCondition())
                tr.AddToSolvingCondition();
            for (int i = 0; i < tr.Customers.Count + 2; i++)
            {
                dtable.Columns.Add();
            }
            dtable.LoadDataRow(this.CreateFirstRow(), false);
            for (int i = 1; i < tr.Providers.Count + 1; i++)
            {
                object[] obj = new object[tr.Customers.Count + 2];
                obj[0] = i;
                for (int j = 1; j <= tr.Customers.Count; j++)
                {
                    if (IsEmpty)
                        obj[j] = 0;
                    else
                        obj[j] = tr.SupportPlan[i - 1, j - 1];
                }
                obj[tr.Customers.Count + 1] = tr.Providers[i - 1];
                dtable.LoadDataRow(obj, true);
            }
            dtable.LoadDataRow(this.CreateLastRow(), false);
            dGrid.ItemsSource = dtable.AsDataView();
            if (!IsEmpty)
                dGrid.IsReadOnly = true;
            else
                dGrid.IsReadOnly = false;
        }
        public void FillCostsDataGrid(bool IsEmpty)
        {
            dGrid.Columns.Clear();
            System.Data.DataTable dtable = new System.Data.DataTable();
            for (int i = 0; i < tr.Customers.Count + 2; i++)
            {
                dtable.Columns.Add();
            }
            dtable.LoadDataRow(this.CreateFirstRow(), false);
            for (int i = 1; i < tr.Providers.Count + 1; i++)
            {
                object[] obj = new object[tr.Customers.Count + 2];
                obj[0] = i;
                for (int j = 1; j <= tr.Customers.Count; j++)
                {
                    if (IsEmpty == true)
                        obj[j] = 0;
                    else
                        obj[j] = tr.Costs[i - 1, j - 1];
                }
                obj[tr.Customers.Count + 1] = tr.Providers[i - 1];
                dtable.LoadDataRow(obj, true);
            }
            dtable.LoadDataRow(this.CreateLastRow(), false);
            dGrid.ItemsSource = dtable.AsDataView();
            if (!IsEmpty)
                dGrid.IsReadOnly = true;
            else
                dGrid.IsReadOnly = false;
        }
        public void FillPotencialDataGrid(List<Point> cycle)
        {
            dGrid.Dispatcher.Invoke(new Action(dGrid.Columns.Clear));
            System.Data.DataTable dtable = new System.Data.DataTable();
            if (!tr.CheckSolvingCondition())
                tr.AddToSolvingCondition();
            for (int i = 0; i < tr.Customers.Count + 2; i++)
            {
                dtable.Columns.Add();
            }
            dtable.LoadDataRow(this.CreateFirstRowPotential(), false);
            for (int i = 1; i < tr.Providers.Count + 1; i++)
            {
                object[] obj = new object[tr.Customers.Count + 2];
                obj[0] = "U" + i + "=" + tr.UPotential[i - 1];

                for (int j = 1; j <= tr.Customers.Count; j++)
                {
                    Point point;
                    obj[j] = tr.SupportPlan[i - 1, j - 1];
                    //point = cycle.Where(p => p.X == i - 1 && p.Y == j - 1).First();
                    point = getPoint(cycle, i - 1, j - 1);
                    if (point.X != -1)
                        if (cycle.IndexOf(point) % 2 == 0)
                            obj[j] += "[" + tr.Potencials[(int)point.Y, (int)point.X] + "]" + "[+]";
                        else
                            obj[j] += "[" + tr.Potencials[(int)point.Y, (int)point.X] + "]" + "[-]";
                }
                obj[tr.Customers.Count + 1] = tr.Providers[i - 1];
                dtable.LoadDataRow(obj, true);
            }
            dtable.LoadDataRow(this.CreateLastRowPotential(), false);
            dGrid.ItemsSource = dtable.AsDataView();
            dGrid.IsReadOnly = true;

        }
        private Point getPoint(List<Point> cycle, int i, int j)
        {
            foreach (Point p in cycle)
                if (p.X == j && p.Y == i)
                    return p;
            return new Point(-1, -1);
        }
        public void GetFromDataGrid(bool IsCost)
        {
            float[,] d = new float[tr.Providers.Count, tr.Customers.Count];
            dGrid.SelectAllCells();
            for (int i = 1; i <= tr.Providers.Count; i++)
            {
                object[] rows = ((DataRowView)dGrid.Items[i]).Row.ItemArray;
                for (int j = 1; j <= tr.Customers.Count; j++)
                {
                    d[i - 1, j - 1] = float.Parse(rows[j].ToString());
                    if (d[i - 1, j - 1] < 0)
                        throw new Exception();
                }
            }
            if (IsCost)
                tr.Costs = d;
            else
            {
                if (!CheckSupPlanGetData(d))
                    throw new FormatException("Неправильно розподілені дані по таблиці перевезень!");
                tr.SupportPlan = d;
            }
        }
        object[] CreateFirstRowPotential()
        {
            object[] obj = new object[tr.Customers.Count + 2];
            obj[0] = "";
            for (int i = 1; i <= tr.Customers.Count; i++)
            {
                obj[i] = "V" + i + "=" + tr.VPotential[i - 1];
            }
            obj[tr.Customers.Count + 1] = "Постачальники";
            return obj;

        }
        object[] CreateFirstRow()
        {
            object[] obj = new object[tr.Customers.Count + 2];
            obj[0] = "";
            for (int i = 1; i <= tr.Customers.Count; i++)
            {
                obj[i] = i;
            }
            obj[tr.Customers.Count + 1] = "Постачальники";
            return obj;
        }
        object[] CreateLastRow()
        {
            object[] obj = new object[tr.Customers.Count + 2];
            obj[0] = "Споживачі";
            for (int i = 1; i <= tr.Customers.Count; i++)
            {
                obj[i] = tr.Customers[i - 1];
            }
            obj[tr.Customers.Count + 1] = "";
            return obj;
        }
        object[] CreateLastRowPotential()
        {
            object[] obj = new object[tr.Customers.Count + 2];
            obj[0] = "Споживачі";
            for (int i = 1; i <= tr.Customers.Count; i++)
            {
                obj[i] = tr.Customers[i - 1];
            }
            obj[tr.Customers.Count + 1] = "";
            return obj;

        }
        bool CheckSupPlanGetData(float[,] data)
        {
            float[] providersSum = new float[tr.Providers.Count];
            float[] customersSum = new float[tr.Customers.Count];
            for (int i = 0; i < tr.Providers.Count; i++)
            {
                for (int j = 0; j < tr.Customers.Count; j++)
                {
                    providersSum[i] += data[i, j];
                    customersSum[j] += data[i, j];
                }
            }

            for (int i = 0; i < tr.Providers.Count; i++)
                if (providersSum[i] != tr.Providers[i])
                    return false;
            for (int i = 0; i < tr.Customers.Count; i++)
                if (customersSum[i] != tr.Customers[i])
                    return false;
            return true;
        }
    }
}
