﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Transport
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataGridAdapter tc = new DataGridAdapter(DGrid_Costs, transport);
            tcosts = tc;
            tc = new DataGridAdapter(DGrid_SupPlan, transport);
            tSupPlan = tc;
            tc = new DataGridAdapter(DGrid_CostsOutput, transport);
            tCostsShow = tc;
        }
        TransportProblem transport = new TransportProblem();
        DataGridAdapter tcosts, tSupPlan, tCostsShow;
        State currentState = State.None;
        enum State { None, NW, Min, Potencials, Monitoring }

        private void BT_MinCost_Click(object sender, RoutedEventArgs e)
        {
            TabC.SelectedIndex = 2;
            currentState = State.Min;
            BTLoad.Visibility = Visibility.Collapsed;
        }

        private void BT_NW_Click(object sender, RoutedEventArgs e)
        {
            TabC.SelectedIndex = 2;
            currentState = State.NW;
            BTLoad.Visibility = Visibility.Collapsed;
        }

        private void BT_To_Input_Costs_Click(object sender, RoutedEventArgs e)
        {
            float[] ResultProvider, ResultCustomer;
            if (TB_input.Text == "")
            {
                MessageBox.Show("ВВедіть споживачів!");
                return;
            }
            if (TB_inputProviders.Text == "")
            {
                MessageBox.Show("ВВедіть Постачальників!");
                return;
            }
            try
            {
                string[] str = TB_input.Text.Split(';');
                ResultProvider = new float[str.Length];
                for (int item = 0; item < str.Length; item++)
                {
                    str[item] = str[item].Trim();
                    float temp = float.Parse(str[item]);
                    if (temp > 0)
                        ResultProvider[item] = temp;
                    else
                        throw new IndexOutOfRangeException("Неможе бути від'ємних значень чи нуля.");
                }
            }
            catch (Exception exp) { MessageBox.Show("Неправильний ввід споживачів!\n" + exp.Message); return; }
            try
            {
                string[] str = TB_inputProviders.Text.Split(';');
                ResultCustomer = new float[str.Length];
                for (int item = 0; item < str.Length; item++)
                {
                    str[item] = str[item].Trim();
                    float temp = float.Parse(str[item]);
                    if (temp > 0)
                        ResultCustomer[item] = temp;
                    else
                        throw new IndexOutOfRangeException("Неможе бути від'ємних значень чи нуля.");
                }

            }
            catch (Exception exp) { MessageBox.Show("Неправильний ввід постачальників!\n" + exp.Message); return; }

            transport.Customers.Clear();
            transport.Providers.Clear();
            transport.AddCustomers(ResultCustomer);
            transport.AddProviders(ResultProvider);

            if (currentState == State.Monitoring)
            {
                TB_DG.Visibility = Visibility.Hidden;
                LableOptCost.Visibility = Visibility.Hidden;
                BT_ToPotential.Visibility = Visibility.Visible;
                tSupPlan.FillDataGrid(true);
                tCostsShow.FillCostsDataGrid(true);

                TabC.SelectedIndex = 1;
                return;
            }
            tcosts.FillCostsDataGrid(true);
            TabC.SelectedIndex = 3;
        }

        private void BT_Prev_ToInput_Click(object sender, RoutedEventArgs e)
        {
            TabC.SelectedIndex = 2;
        }

        private void Monitoring_Click(object sender, RoutedEventArgs e)
        {
            currentState = State.Monitoring;
            TabC.SelectedIndex = 2;
            BTLoad.Visibility = Visibility.Collapsed;
        }

        private void BTLoad_Click(object sender, RoutedEventArgs e)
        {
            if (transport.Load("ser"))
            {
                tSupPlan.FillDataGrid(false);
                tCostsShow.FillCostsDataGrid(false);
                TB_DG.Text = transport.CostsSumByPlan().ToString();
                BTLoad.Visibility = Visibility.Collapsed;
                TabC.SelectedIndex = 1;
            }
            else
                MessageBox.Show("Щось пішло не так! Виконати дію неможливо!");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            transport.Save("ser");
        }

        private void BT_Next_ToGrid_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tcosts.GetFromDataGrid(true);
            }
            catch
            {
                MessageBox.Show(" Щось введено не вірно! Значення цін не мають бути від'ємними чи містити зайві символи окрім цифр і точки.");
                return;
            }
            transport.AddToSolvingCondition();
            if (currentState == State.NW)
                transport.NW_SupportPlan();
            if (currentState == State.Min)
                transport.MinCost_SupportPlan();

            bool emptyGrids = false;
            if (currentState == State.Monitoring)
            {
                emptyGrids = true;
                TB_DG.Visibility = Visibility.Hidden;
                LableOptCost.Visibility = Visibility.Hidden;
            }

            tSupPlan.FillDataGrid(emptyGrids);
            tCostsShow.FillCostsDataGrid(emptyGrids);

            TB_DG.Visibility = Visibility.Visible;
            LableOptCost.Visibility = Visibility.Visible;
            TB_DG.Text = transport.CostsSumByPlan().ToString();

            if (BT_ToPotential.Visibility == Visibility.Hidden)
                BT_ToPotential.Visibility = Visibility.Visible;
            TabC.SelectedIndex = 1;
        }

        private void BT_ToPotential_Click(object sender, RoutedEventArgs e)
        {
            if (currentState == State.Monitoring)
            {
                try { tCostsShow.GetFromDataGrid(true); }
                catch
                {
                    MessageBox.Show(" Щось введено не вірно! Значення цін не мають бути від'ємними чи містити зайві символи окрім цифр і точки.");
                    return;
                }
                try { tSupPlan.GetFromDataGrid(false); }
                catch (Exception ex)
                {
                    MessageBox.Show(" Щось введено не вірно! Значення перевезень не мають бути від'ємними чи містити зайві символи окрім цифр і точки.\n" + ex.Message);
                    return;
                }
                transport.CheckDegenerate();
                transport.PotentailsCalculate();
                if (transport.CheckOptimalSolution())
                {
                    MessageBox.Show(" Рішення оптимальне!");
                }
            }
            currentState = State.Potencials;

            PotencialWindow potencialWindow = new PotencialWindow(transport.DeepCopy());
            potencialWindow.ShowDialog();
            //TODO, add PotencialWindow
            transport.OptimalSolution();
            tSupPlan.FillDataGrid(false);
            tCostsShow.FillCostsDataGrid(false);

            TB_DG.Visibility = Visibility.Visible;
            LableOptCost.Visibility = Visibility.Visible;
            TB_DG.Text = transport.CostsSumByPlan().ToString();
            BT_ToPotential.Visibility = Visibility.Hidden;
        }

    }
}
